package tests;

import main.entities.Auction;
import main.entities.AuctionedObject;
import main.entities.Buyer;
import junit.framework.TestCase;
import main.exceptions.AuctionException;
import main.exceptions.InvalidAuctionedObjectException;
import main.exceptions.InvalidBidException;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Andrei on 28-Jul-18.
 * Test case for Buyer.
 */
public class BuyerTest extends TestCase {

    private Buyer buyer;

    @Before
    public void setUp(){
        this.buyer = new Buyer("Buyer-Test1");
    }

    @Test
    public void testInitialization(){
        assertNotNull(buyer.getBids());
        assertNotNull(buyer.getId());
        assertNotNull(buyer.getName());
        assertEquals(buyer.getName(), "Buyer-Test1");
    }

    @Test
    public void testHighestBid(){
        try{
            Double highestBid = 99d;
            Auction auction = new Auction(new AuctionedObject("Object", 10));
            auction.placeBid(buyer, 10d, 52d, highestBid, 12d, 16d);
            assertEquals(highestBid, buyer.getHighestBid());
        }catch (InvalidAuctionedObjectException | InvalidBidException | AuctionException ignored){}
    }

}
