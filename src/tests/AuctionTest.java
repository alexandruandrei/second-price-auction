package tests;

import junit.framework.TestCase;
import main.entities.Auction;
import main.entities.AuctionedObject;
import main.entities.Buyer;
import main.exceptions.AuctionException;
import main.exceptions.InvalidAuctionedObjectException;
import main.exceptions.InvalidBidException;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Andrei on 28-Jul-18.
 * Test case for Auction.
 */
public class AuctionTest{

    private AuctionedObject auctionedObject;
    private Auction auction;

    @Before
    public void setUp(){
        try{
            this.auctionedObject = new AuctionedObject("AuctionedObject-Test1", 100d);
            this.auction = new Auction(auctionedObject);
        }catch (InvalidAuctionedObjectException iaoe){
            fail(iaoe.getMessage());
        }
    }

    @Test
    public void testInitialization(){
        try{
            Auction auction = new Auction(null);
            fail("AuctionedObject - null. An invalid auctioned object exception should've been thrown.");
        }catch (InvalidAuctionedObjectException ignored){}
        try{
            AuctionedObject auctionedObject = new AuctionedObject("Object", -1);
            Auction auction = new Auction(auctionedObject);
            fail("AuctionedObject price - invalid. An invalid auctioned object exception should've been thrown.");
        }catch (InvalidAuctionedObjectException ignored){}
        try{
            AuctionedObject auctionedObject = new AuctionedObject(null, 1);
            Auction auction = new Auction(auctionedObject);
            fail("AuctionedObject name - invalid. An invalid auctioned object exception should've been thrown.");
        }catch (InvalidAuctionedObjectException ignored){}

    }

    @Test
    public void testInvalidBid(){
        Buyer b1 = new Buyer("B1");
        try{
            auction.placeBid(b1, -1d);
            fail("Bid value - negative. An invalid bid exception should've been thrown.");
        }catch (InvalidBidException | AuctionException ignored){}
    }

    @Test
    public void testBidPlacement(){
        Buyer b1 = new Buyer("B1");
        Buyer b2 = new Buyer("B2");
        Buyer b3 = new Buyer("B3");

        try{
            auction.placeBid(b1, 100d, 110d, 115d);
            auction.placeBid(b2);
            auction.placeBid(b3, 105d, 140d);

            assertEquals(3, b1.getBids().size());
            assertEquals(0, b2.getBids().size());
            assertEquals(2, b3.getBids().size());

        }catch (InvalidBidException | AuctionException ibe){
            fail(ibe.getMessage());
        }
    }

    @Test
    public void testWinnerPick(){
        Buyer buyerA = new Buyer("buyer-A");
        Buyer buyerB = new Buyer("buyer-B");
        Buyer expectedWinner = new Buyer("buyer-C");
        Double expectedWinningPrice = auctionedObject.getReservedPrice();
        try{
            auction.placeBid(buyerA, null);
            auction.placeBid(buyerB);
            auction.placeBid(expectedWinner, 140d);
            Map<Buyer, Double> resultMap = auction.endAuction();

            if(resultMap.isEmpty()){
                fail("No winner has been chosen.");
            }

            Map.Entry<Buyer, Double> result = resultMap.entrySet().iterator().next();
            assertEquals(expectedWinner, result.getKey());
            assertEquals(expectedWinningPrice, result.getValue());

        }catch (InvalidBidException | AuctionException ibe){
            fail(ibe.getMessage());
        }
    }

    @Test
    public void testTieWinnerPick(){
        Buyer expectedWinner = new Buyer("buyer-A");
        Buyer buyerB = new Buyer("buyer-B");
        Double expectedWinningPrice = 110d;

        try{
            auction.placeBid(expectedWinner, 110d);
            auction.placeBid(buyerB, 110d);

            Map<Buyer, Double> resultMap = auction.endAuction();
            if(resultMap.isEmpty()){
                fail("No winner has been chosen.");
            }

            Map.Entry<Buyer, Double> result = resultMap.entrySet().iterator().next();
            assertEquals(expectedWinner, result.getKey());
            assertEquals(expectedWinningPrice, result.getValue());

        }catch (InvalidBidException | AuctionException ibe){
            fail(ibe.getMessage());
        }
    }

    @Test
    public void testSingleBidder(){
        Buyer expectedWinner = new Buyer("buyer-A");
        Double expectedWinningPrice = auctionedObject.getReservedPrice();

        try{
            auction.placeBid(expectedWinner, 110d);
            Map<Buyer, Double> resultMap = auction.endAuction();
            if(resultMap.isEmpty()){
                fail("No winner has been chosen.");
            }
            Map.Entry<Buyer, Double> result = resultMap.entrySet().iterator().next();
            assertEquals(expectedWinner, result.getKey());
            assertEquals(expectedWinningPrice, result.getValue());

        }catch (InvalidBidException | AuctionException ae){
            fail(ae.getMessage());
        }
    }
}