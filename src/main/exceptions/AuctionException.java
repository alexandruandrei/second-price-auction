package main.exceptions;

/**
 * Created by Andrei on 28-Jul-18.
 * Auction exception.
 *
 * Thrown when an invalid operation is performed on auction.
 */
public class AuctionException extends Exception {

    public AuctionException(String message){
        super(message);
    }

}
