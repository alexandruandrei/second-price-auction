package main.exceptions;

/**
 * Created by Andrei on 28-Jul-18.
 * Invalid bid exception.
 *
 * Thrown when an invalid bid is placed for an auctioned object.
 */
public class InvalidBidException extends Exception{

    public InvalidBidException(String message){
        super(message);
    }

}
