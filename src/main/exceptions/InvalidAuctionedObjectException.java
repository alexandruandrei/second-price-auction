package main.exceptions;

/**
 * Created by Andrei on 28-Jul-18.
 * Invalid auctioned object exception.
 *
 * Thrown when an invalid auction object is added to an auction.
 */
public class InvalidAuctionedObjectException extends Exception {

     public InvalidAuctionedObjectException(String message){
         super(message);
     }

}
