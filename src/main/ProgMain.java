package main;


import main.entities.Auction;
import main.entities.AuctionedObject;
import main.entities.Buyer;

/**
 * Created by Andrei on 28-Jul-18.
 * Main class used to interact with 2nd Price Auction.
 */
public class ProgMain {

    public static void main(String[] args) throws Exception{

        AuctionedObject lampAuctionedObject = new AuctionedObject("Antique lamp",
                100);
        Auction auction = new Auction(lampAuctionedObject);

        Buyer buyerA = new Buyer("A");
        Buyer buyerB = new Buyer("B");
        Buyer buyerC = new Buyer("C");
        Buyer buyerD = new Buyer("D");
        Buyer buyerE = new Buyer("E");

        auction.placeBid(buyerA, 110d, 130d);
        auction.placeBid(buyerB);
        auction.placeBid(buyerC, 125d);
        auction.placeBid(buyerD, 105d, 115d, 90d);
        auction.placeBid(buyerE, 132d, 135d, 140d);

        auction.printBuyers();

        auction.endAuction();
    }

}
