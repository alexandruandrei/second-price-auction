package main.entities;

import java.util.TreeSet;

/**
 * Created by Andrei on 28-Jul-18.
 * Auction Buyer.
 */
public class Buyer implements Comparable<Buyer>{

    private static int idCount = 0;

    private Integer id;
    private String name;
    private TreeSet<Double> bids;

    public Buyer(String name){
        this.id = createId();
        this.name = name;
        this.bids = new TreeSet<>();
    }

    /**
     * Generates an unique ID for a buyer.
     * @return buyer id
     */
    private static synchronized int createId(){
        return idCount++;
    }

    /**
     * Get the highest bid placed.
     * If buyer placed no bids returns -1
     * @return highestBid
     */
    public Double getHighestBid(){
        if(!bids.isEmpty()){
            return bids.last();
        }
        return -1d;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TreeSet<Double> getBids() {
        return bids;
    }

    @Override
    public int compareTo(Buyer buyer) {
        if(this.getHighestBid()< buyer.getHighestBid()){
            return -1;
        }else if(this.getHighestBid() > buyer.getHighestBid()){
            return 1;
        }else{
            if(this.getId() < buyer.getId()){
                return 1;
            }else {
                return -1;
            }
        }
    }

    @Override
    public boolean equals(Object b) {
        if (this == b) return true;
        if (b == null || getClass() != b.getClass()) {
            return false;
        }
        Buyer buyer = (Buyer) b;
        return id != null ? id.equals(buyer.id) : buyer.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Buyer {" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", bids=" + bids +
                '}';
    }
}
