package main.entities;

import com.sun.istack.internal.NotNull;

/**
 * Created by Andrei on 28-Jul-18.
 */
public class AuctionedObject {

    private String name;
    private Double reservedPrice;

    public AuctionedObject(@NotNull String name, @NotNull Double reservedPrice){
        this.name = name;
        this.reservedPrice = reservedPrice;
    }

    public AuctionedObject(@NotNull String name, @NotNull Integer reservedPrice){
        this.name = name;
        this.reservedPrice = reservedPrice != null ? reservedPrice.doubleValue() : null;
    }

    public String getName() {
        return name;
    }

    public Double getReservedPrice() {
        return reservedPrice;
    }

    @Override
    public String toString() {
        return "AuctionedObject {" +
                "name='" + name + '\'' +
                ", reservedPrice=" + reservedPrice +
                '}';
    }
}
