package main.entities;

import com.sun.istack.internal.NotNull;
import main.exceptions.AuctionException;
import main.exceptions.InvalidAuctionedObjectException;
import main.exceptions.InvalidBidException;

import java.util.*;

/**
 * Created by Andrei on 28-Jul-18.
 * Auction
 */
public class Auction {

    private boolean auctionOngoing;
    private AuctionedObject auctionedObject;
    private TreeSet<Buyer> buyers;

    public Auction(@NotNull AuctionedObject auctionedObject) throws InvalidAuctionedObjectException {
        if(auctionedObject == null){
            throw new InvalidAuctionedObjectException("Auctioned object cannot be null");
        }else if(auctionedObject.getReservedPrice() == null || auctionedObject.getReservedPrice() <= 0){
            throw new InvalidAuctionedObjectException("Auctioned object price must be greater than 0");
        }else if(auctionedObject.getName() == null || auctionedObject.getName().trim().isEmpty()){
            throw new InvalidAuctionedObjectException("Auctioned object must have a name");
        }
        this.auctionedObject = auctionedObject;
        this.buyers = new TreeSet<>();
        this.auctionOngoing = true;
    }

    /**
     * Places a bid for the auctioned object.
     *
     * @param buyer - buyer that places bid.
     * @param bids - bids to be placed. Each value must be greater or equal to reserved price.
     */
    public void placeBid(@NotNull Buyer buyer, Double ...bids) throws InvalidBidException, AuctionException{
        if(auctionOngoing){
            if(buyer == null){
                throw new InvalidBidException("Buyer cannot be null");
            }
            if(bids != null){
                for(Double bid:bids){
                    if(bid < 0){
                        throw new InvalidBidException("Placed bid(s) cannot be negative");
                    }
                }
            }

            System.out.println("-> Buyer " + buyer.getName()
                    + " enters the auction bidding: "
                    + (bids!=null && bids.length >0 ?Arrays.asList(bids):"nothing"));

            if(!buyers.contains(buyer)){
                if(bids!=null){
                    buyer.getBids().addAll(Arrays.asList(bids));
                }
                buyers.add(buyer);
            }else if(bids!=null){
                buyers.forEach((b)->{
                    if(b.getId().equals(buyer.getId())){
                        buyer.getBids().addAll(Arrays.asList(bids));
                    }
                });
            }
        }else{
            throw new AuctionException("Auction ended and no longer accepting bids.");
        }
    }

    /**
     * End auction and pick a winner with a winning price.
     * @return
     */
    public Map<Buyer, Double> endAuction() throws AuctionException{
        if(buyers.isEmpty()){
            throw new AuctionException("No winner bidden for the auctioned object!");
        }
        int noBidsPlaced = 0;
        for(Buyer b:buyers){
            if(b.getBids().isEmpty()){
                noBidsPlaced++;
            }
        }
        if(noBidsPlaced == buyers.size()){
            throw new AuctionException("No bids placed for the auctioned object!");
        }

        // Pick winning buyer
        Buyer winningBuyer = buyers.last();

        // Pick winning price
        Double winningPrice;
        if(buyers.size()>=2){
            TreeSet<Buyer> buyersCopy = new TreeSet<>(buyers);
            buyersCopy.removeIf(b -> b.getId().equals(winningBuyer.getId()));
            winningPrice = buyersCopy.last().getHighestBid();
        }else{
            winningPrice = auctionedObject.getReservedPrice();
        }

        if(winningPrice < auctionedObject.getReservedPrice()){
            winningPrice = auctionedObject.getReservedPrice();
        }

        Map<Buyer, Double> winner = new LinkedHashMap<>();
        winner.put(winningBuyer, winningPrice);

        System.out.println("-> Winner: buyer" + winningBuyer.getName() + " with price: " + winningPrice + "\n");

        auctionOngoing = false;
        return winner;
    }

    /**
     * Prints the auctioned object and bidders.
     */
    public void printBuyers(){
        System.out.println("\n-> " + auctionedObject);
        System.out.println("-> Current auction bidders: " + buyers.size());
        buyers.forEach((buyer)->
                System.out.println("\t" + buyer)
        );
    }

    public boolean isAuctionOngoing() {
        return auctionOngoing;
    }

}
